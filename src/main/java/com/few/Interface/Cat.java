/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.Interface;

/**
 *
 * @author f
 */
public class Cat extends LandAnimal{
        private String nickname;

    public Cat(String nickname) {
        super(nickname, 4);
        this.nickname = nickname;
    }
    

    @Override
    public void run() {
        System.out.println("Cat: "+nickname+" run");
    }

    @Override
    public void eat() {
        System.out.println("Cat: "+nickname+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Cat: "+nickname+" walk");
    }

    @Override
    public void speak() {
        System.out.println("Cat: "+nickname+" speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: "+nickname+" sleep");
    }
    
    public boolean behavior() {
        run();
        eat();
        walk();
        speak();
        sleep();
        crawl();
        swim();
        System.out.println("________________________");
        return true;
    }

    @Override
    public void crawl() {
        System.out.println("Cat: " + nickname + " crawl");
    }

    @Override
    public void swim() {
        System.out.println("Cat: " + nickname + " swim");
    }
}
