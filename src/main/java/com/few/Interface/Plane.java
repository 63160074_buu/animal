/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.Interface;

/**
 *
 * @author f
 */
public class Plane extends Vehicle implements Flyable, Runable {

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: Star engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: Stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: Raise speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: Apply break");
    }

    @Override
    public void fly() {
        System.out.println("Plane: Fly");
    }

    @Override
    public void run() {
        System.out.println("Plant: Run");
    }

    public boolean working() {
        startEngine();
        run();
        raiseSpeed();
        fly();
        applyBreak();
        stopEngine();
        System.out.println("________________________");
        return true;
    }

}
