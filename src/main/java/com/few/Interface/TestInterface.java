/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.Interface;

/**
 *
 * @author f
 */
public class TestInterface {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.behavior();
        Cat c1 = new Cat("Ping");
        c1.behavior();
        Dog d1 = new Dog("Pong");
        d1.behavior();
        Crocodile cr1 = new Crocodile("Croc");
        cr1.behavior();
        Snake s1 = new Snake("Long");
        s1.behavior();
        Fish f1 = new Fish("Gold");
        f1.behavior();
        Crab cb1 = new Crab("Som");
        cb1.behavior();
        Bird bd1 = new Bird("Gaw");
        bd1.behavior();
        Bat bt1 = new Bat("Bun");
        bt1.behavior();
        Plane p1 = new Plane("Engine NO.1");
        p1.working();
        Car car1 = new Car("Engine NO.1");
        car1.working();
    }
}
