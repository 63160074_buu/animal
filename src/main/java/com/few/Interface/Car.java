/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.Interface;

/**
 *
 * @author f
 */
public class Car extends Vehicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car: Star engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: Stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car: Raise speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car: Apply break");
    }

    @Override
    public void run() {
        System.out.println("Car: Run");
    }

    public boolean working() {
        startEngine();
        run();
        raiseSpeed();
        applyBreak();
        stopEngine();
        System.out.println("________________________");
        return true;
    }

}
