/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.Interface;

/**
 *
 * @author f
 */
public class Bat extends Poulty {

    private String nickname;

    public Bat(String nickname) {
        super(nickname, 2, 2);
        this.nickname = nickname;
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + nickname + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + nickname + " sleep");
    }

    public boolean behavior() {
        fly();
        eat();
        walk();
        speak();
        sleep();
        System.out.println("________________________");
        return true;
    }
}
