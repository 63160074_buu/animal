/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.Interface;

/**
 *
 * @author f
 */
public class Dog extends LandAnimal {

    private String nickname;

    public Dog(String nickname) {
        super(nickname, 4);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Dog: " + nickname + " run");
    }

    @Override
    public void eat() {
        System.out.println("Dog: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Dog: " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog: " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: " + nickname + " sleap");
    }

    public boolean behavior() {
        run();
        eat();
        walk();
        speak();
        sleep();
        crawl();
        swim();
        System.out.println("________________________");
        return true;
    }

    @Override
    public void crawl() {
        System.out.println("Dog: " + nickname + " crawl");
    }

    @Override
    public void swim() {
        System.out.println("Dog: " + nickname + " swim");
    }
}
